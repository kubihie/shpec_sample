. ./sample.sh

describe '引数チェック関数のテスト'
  it "引数が２つの場合は0を返すこと"
    ret=$(check_args_count 1 2)
    assert equal $ret 0
  end
  it "引数が２つで無い場合は1を返すこと"
    ret=$(check_args_count)
    assert equal $ret 1
    ret=$(check_args_count 1)
    assert equal $ret 1
    ret=$(check_args_count 1 2 3)
    assert equal $ret 1
  end
end

describe "引数が実数であるかのテスト"
  it "引数が実数の場合は0を返すこと"
    ret=$(check_arg_is_number 1)
    assert equal $ret 0
  end
  it "引数が文字列の場合は1を返すこと"
    ret=$(check_arg_is_number a)
    assert equal $ret 1
  end
  it "引数が小数の場合は1を返すこと"
    ret=$(check_arg_is_number 1.001)
    assert equal $ret 1
  end
end

describe "足し算関数のテスト"
  it "1+2が3になること"
    ret=$(tasi 1 2)
    assert equal $ret 3
  end
end

describe "エラーメッセージのテスト"
  it "引数が２つでない場合のエラーメッセージ表示の確認"
    ret=$(main 1 2 3)
    assert equal $ret '引数が２つではありませんでした'
  end
  it "引数が数値でない場合のエラーメッセージ表示の確認"
    ret=$(main hoge fuga)
    assert egrep $ret "引数:.*は数値ではありませんでした"
    ret=$(main 1 fuga)
    assert egrep $ret "引数:.*は数値ではありませんでした"
    ret=$(main hoge 1)
    assert egrep $ret "引数:.*は数値ではありませんでした"
  end
end
