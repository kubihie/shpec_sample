#!/bin/bash

check_args_count(){
  if [ $# -eq 2 ]; then
    echo 0
  else
    echo 1
  fi
}

check_arg_is_number(){
  expr $1 + 1 > /dev/null 2>&1
  if [ $? -lt 2 ]; then
    echo 0
  else
    echo 1
  fi
}

tasi(){
  echo $(expr $1 + $2)
}

main(){
  ret=$(check_args_count $@)
  if [ $ret -ne 0 ]; then
    echo '引数が２つではありませんでした'
    exit 1
  fi

  for arg in $@; do
    ret=$(check_arg_is_number ${arg})
    if [ $ret -ne 0 ]; then
      echo "引数:${arg}は数値ではありませんでした"
      exit 2
    fi
  done

  tasi_result=$(tasi $@)
  echo "足し算の結果は${tasi_result}でした"
}

# メイン処理呼び出し
[ ! $(echo $0 | grep 'shpec') ] && main "$@"
